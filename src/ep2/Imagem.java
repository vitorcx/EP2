
package ep2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Integer.parseInt;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Imagem {
    
    private String numeroMagico;
    private String comentario;
    private int largura;
    private int altura;
    private int corMaxima;
    private String largAlt;
    
    public Imagem(){
        numeroMagico = "Numero Magico";
        comentario = "Construtor nulo";
        largura = 0;
        altura = 0;
        corMaxima = 255;
    };
    public Imagem(String numeroMagico, String comentario, int largura, int altura, int corMaxima){
        
    };
    
    public String getNumeroMagico(){
        return numeroMagico;
    };
    public void setNumeroMagico(String numeroMagico){
        this.numeroMagico = numeroMagico;
    };
    
    public String getComentario(){
        return comentario;
    };
    public void setComentario(String comentario){
        this.comentario = comentario;
    };
    
    public void setLargAlt(String largAlt){
        this.largAlt = largAlt;
    }
    
    public String getLargAlt(){
        return largAlt;
    }
    
    public int getLargura(){
        return largura;
    };
    public void setLargura(int largura){
        this.largura = largura;
    };
    
    public int getAltura(){
        return altura;
    };
    public void setAltura(int altura){
        this.altura = altura;
    };
    
    public int getCorMaxima(){
        return corMaxima;
    };
    public void setCorMaxima(int corMaxima){
        this.corMaxima = corMaxima;
    };
    
    public String carregaImagem(String nomeDaImagem){ //string
        //String fileName = new String();
        String conteudoImagem = new String();
        try {
            FileInputStream arquivoImagem = new FileInputStream("/home/vitor/Downloads/lena.pgm");
            InputStreamReader input = new InputStreamReader(arquivoImagem);
            BufferedReader br = new BufferedReader(input);
            
            
            //Estrutura inicial de leitura
            /*do{
                try {
                    conteudoImagem = br.readLine();
                    if(conteudoImagem != null){
                        setNumeroMagico(conteudoImagem);
                        System.out.println(conteudoImagem);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
                }
            }while(conteudoImagem != null);*/
            
            //Estrutura 2
            try{
                conteudoImagem = br.readLine();
                if(conteudoImagem != null){
                    setNumeroMagico(conteudoImagem);
                    //System.out.println("Numero mágico: " + getNumeroMagico());
                }
                conteudoImagem = br.readLine();
                if(conteudoImagem != null){
                    setComentario(conteudoImagem);
                    //System.out.println("Comentario: " + getComentario());
                }
                conteudoImagem = br.readLine();
                if(conteudoImagem != null){
                    String tamanho;
                    tamanho = conteudoImagem;
                    setLargAlt(tamanho);
                    String[] stringSeparada = tamanho.split(" ");
                    setLargura(parseInt(stringSeparada[0]));
                    setAltura(parseInt(stringSeparada[1]));
                    //System.out.println("Largura: " + getLargura() + "Altura: " + getAltura());
                    
                }
                conteudoImagem = br.readLine();
                if(conteudoImagem != null){
                    setCorMaxima(parseInt(conteudoImagem));
                    //System.out.println("Cor Maxima :" + getCorMaxima());
                }
                
                //Pega linha vazia
                conteudoImagem = br.readLine();
                
                if("P5".equals(getNumeroMagico())){
                    conteudoImagem = br.readLine();
                }else if("P6".equals(getNumeroMagico())){
                    while(!br.readLine().isEmpty()){ //risco de erro ***
                        conteudoImagem += br.read();                                                
                    }
                }else {
                    System.out.println("Erro ao retornar pixels do arquivo de entrada!");
                    System.out.println("O programa será encerrado!");
                    System.exit(0);
                }
                
                
            } catch(IOException ex) { // catch do try do bufferedReader
                Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                arquivoImagem.close();
            } catch (IOException ex) {
                Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } catch (FileNotFoundException ex) { //catch do try de abertura do arquivo
            Logger.getLogger(Imagem.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Arquivo não encontrado");
        }  
        return conteudoImagem;
    };
    
}

