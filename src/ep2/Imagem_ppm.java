
package ep2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Imagem_ppm extends Imagem {
    
    private Pixel pix;
    
    public void Imagem_ppm(){
        setNumeroMagico("NumeroMagico");
        setComentario("Comentario");
        setLargAlt("Tamanho");
        setLargura(0);
        setAltura(0);
        setCorMaxima(0);
    }
    
    public void Imagem_ppm(String numeroMagico, String comentario, String largAlt, int largura, int altura, int corMaxima, Pixel[] pix){
        setNumeroMagico(numeroMagico);
        setComentario(comentario);
        setLargAlt(largAlt);
        setLargura(largura);
        setAltura(altura);
        setCorMaxima(corMaxima);
        setPixel(pix); 
    }
    
    public void Imagem_ppm(String numeroMagico, String comentario, String largAlt, int largura, int altura, int corMaxima){
        setNumeroMagico(numeroMagico);
        setComentario(comentario);
        setLargAlt(largAlt);
        setLargura(largura);
        setAltura(altura);
        setCorMaxima(corMaxima);
    }
    
    public void decifraPPM(String nomeNovaImagem, String pixels){
        aplicaFiltro(nomeNovaImagem, pixels);
    }
    
    public void inserePixObjeto(String pixels){
        int tamanhoPixels = pixels.length();
        Pixel[] pix;
        pix = new Pixel[tamanhoPixels];
        int i = 0;
        int j = 0;
        
        while(i < tamanhoPixels){
            pix[j].setR(pixels.toCharArray()[i]);
            i++;
            pix[j].setG(pixels.toCharArray()[i]);
            i++;
            pix[j].setB(pixels.toCharArray()[i]);
            i++;
            j++; 
        }
    }
    public void aplicaFiltro(String nomeNovaImagem, String pixels){
        inserePixObjeto(pixels);
        File novaImagem = new File("/home/vitor/Downloads/novaImagem.ppm");
        if(!novaImagem.exists()){
            try {
                novaImagem.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(Imagem_ppm.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Erro ao criar arquivo da nova imagem");
            }
        }
        try {
            FileOutputStream novaImagemOut = new FileOutputStream(novaImagem);
            BufferedOutputStream out = new BufferedOutputStream(novaImagemOut);
            try {
                out.write(getNumeroMagico().getBytes());
                out.write(getComentario().getBytes());
                out.write(getLargAlt().getBytes());
                out.write(getCorMaxima().getBytes());
            } catch (IOException ex) {
                Logger.getLogger(Imagem_ppm.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Imagem_ppm.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public Pixel transformaImagem(char cor, Pixel unicoPix){
        
    }
    
    public void inserePixArquivo(Pixel[] novoPix /*ofstream[] novaImagem*/){
        
    }
    
    public Pixel[] getPixel(){
        return pix;
    }
    
    public void setPixel(Pixel[] pix){
        this.pix = pix;
    }
    
    
    
    
    
    
    
    
    /*try {
            FileOutputStream novaImagem = new FileOutputStream("/home/vitor/Downloads/novaImagem.ppm");
            BufferedOutputStream out = new BufferedOutputStream(novaImagem);
            ;
            } catch (FileNotFoundException ex) {
            Logger.getLogger(Imagem_ppm.class.getName()).log(Level.SEVERE, null, ex);
            }*/
    
    
}
