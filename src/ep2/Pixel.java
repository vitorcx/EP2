
package ep2;


public class Pixel {
    
    private char r;
    private char g;
    private char b;
    
    public void Pixel(){
        setR('r');
        setG('g');
        setB('b');
    }
    
    public void Pixel(char r, char g, char b){
        setR(r);
        setG(g);
        setB(b);
    }
    
    public char getR(){
        return r;
    }
    
    public void setR(char r){
        this.r = r;
    }
    
    public char getG(){
        return g;
    }
    
    public void setG(char g){
        this.g = g;
    }
    
    public char getB(){
        return b;
    }
    
    public void setB(char b){
        this.b = b;
    }
    
}
